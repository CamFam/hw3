from flask import Flask, jsonify, request
import json

# The main Flask app
app = Flask(__name__)

# Data from a json file
data = json.load(open('coe332.json', 'r'))

#home route
@app.route('/')
def coe332():
    return jsonify(data)

	# Instructor routes
@app.route('/instructors')
def get_instructors():
	return jsonify(data['instructors'])

@app.route('/instructors/<int:id>')
def get_instructor(id):
	return jsonify(data['instructors'][id])

@app.route('/instructors/<int:id>/<string:field>')
def get_instructor_name(id, field):
	return jsonify(data['instructors'][id][field])

	# Meetings routes
@app.route('/meeting')
def get_meeting():
	return jsonify(data['meeting'])

@app.route('/meeting/days')
def get_meeting_days():
	return jsonify(data['meeting']['days'])

@app.route('/meeting/start')
def get_meeting_start():
	return jsonify(data['meeting']['start'])

@app.route('/meeting/end')
def get_meeting_end():
	return jsonify(data['meeting']['end'])

@app.route('/meeting/location')
def get_meeting_location():
	return jsonify(data['meeting']['location'])

	# Assignments routes
@app.route('/assignments')
def get_assignments():
	return jsonify(data['assignments'])

@app.route('/assignments/<int:number>')
def get_assignment_number(number):
	return jsonify(data['assignments'][number-1])

@app.route('/assignments/<int:number>/name')
def get_assignment_name(number):
	return jsonify(data['assignments'][number-1]['name'])

@app.route('/assignments/<int:number>/url')
def get_assignment_URL(number):
	return jsonify(data['assignments'][number-1]['url'])

@app.route('/assignments/<int:number>/points')
def get_assignment_points(number):
	return jsonify(data['assignments'][number-1]['points'])

@app.route('/assignments', methods=['POST'])
def post_assignment():
	request_data = request.data.decode("utf-8")
	request_dictionary = json.loads(request_data)
	data['assignments'].append(request_dictionary)
	return jsonify(request_dictionary)
